package handlers

import (
	"context"

	"github.com/gin-gonic/gin"

	"uu_go_api_gateway/api/http"
	"uu_go_api_gateway/api/models"
	"uu_go_api_gateway/genproto/user_service"
	"uu_go_api_gateway/pkg/helper"
	"uu_go_api_gateway/pkg/util"
)

// CreateHobby godoc
// @ID create_hobby
// @Router /hobby [POST]
// @Summary Create Hobby
// @Description  Create Hobby
// @Tags Hobby
// @Accept json
// @Produce json
// @Param profile body user_service.CreateHobby true "CreateHobbyRequestBody"
// @Success 200 {object} http.Response{data=user_service.User} "GetHobbyBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateHobby(c *gin.Context) {

	var hobby user_service.CreateHobby

	err := c.ShouldBindJSON(&hobby)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.HobbyService().Create(
		c.Request.Context(),
		&hobby,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetHobbyByID godoc
// @ID get_hobby_by_id
// @Router /hobby/{id} [GET]
// @Summary Get Hobby  By ID
// @Description Get Hobby  By ID
// @Tags Hobby
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=user_service.Hobby} "HobbyBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetByIDHobby(c *gin.Context) {

	hobbyID := c.Param("id")

	if !util.IsValidUUID(hobbyID) {
		h.handleResponse(c, http.InvalidArgument, "hobby id is an invalid uuid")
		return
	}

	resp, err := h.services.HobbyService().GetByID(
		context.Background(),
		&user_service.HobbyPrimaryKey{
			Id: hobbyID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetHobbyList godoc
// @ID get_hobby_list
// @Router /hobby [GET]
// @Summary Get Hobby s List
// @Description  Get Hobby s List
// @Tags Hobby
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=user_service.GetListHobbyResponse} "GetAllHobbyResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetListHobby(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.HobbyService().GetList(
		context.Background(),
		&user_service.GetListHobbyRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateHobby godoc
// @ID update_hobby
// @Router /hobby/{id} [PUT]
// @Summary Update Hobby
// @Description Update Hobby
// @Tags Hobby
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body user_service.UpdateHobby true "UpdateHobbyRequestBody"
// @Success 200 {object} http.Response{data=user_service.Hobby} "Hobby data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateHobby(c *gin.Context) {

	var hobby user_service.UpdateHobby

	hobby.Id = c.Param("id")

	if !util.IsValidUUID(hobby.Id) {
		h.handleResponse(c, http.InvalidArgument, "hobby id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&hobby)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.HobbyService().Update(
		c.Request.Context(),
		&hobby,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchHobby godoc
// @ID patch_hobby
// @Router /hobby/{id} [PATCH]
// @Summary Patch Hobby
// @Description Patch Hobby
// @Tags Hobby
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=user_service.User} "User data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchHobby(c *gin.Context) {

	var UpdatePatchHobby models.UpdatePatch

	err := c.ShouldBindJSON(&UpdatePatchHobby)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	UpdatePatchHobby.ID = c.Param("id")

	if !util.IsValidUUID(UpdatePatchHobby.ID) {
		h.handleResponse(c, http.InvalidArgument, "hobby id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(UpdatePatchHobby.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.HobbyService().UpdatePatch(
		c.Request.Context(),
		&user_service.UpdatePatchHobby{
			Id:     UpdatePatchHobby.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteHobby godoc
// @ID delete_hobby
// @Router /hobby/{id} [DELETE]
// @Summary Delete Hobby
// @Description Delete Hobby
// @Tags Hobby
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Hobby data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteHobby(c *gin.Context) {

	hobbyId := c.Param("id")

	if !util.IsValidUUID(hobbyId) {
		h.handleResponse(c, http.InvalidArgument, "hobby id is an invalid uuid")
		return
	}

	resp, err := h.services.HobbyService().Delete(
		c.Request.Context(),
		&user_service.HobbyPrimaryKey{Id: hobbyId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
